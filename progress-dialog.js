(function(factory){
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define(["jquery", "underscore", "jquery-ui" ], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS:
        require("jquery-ui")
        exports.ProgressDialog = factory( require('jquery'), require("underscore") );
      } else {
        // Browser globals:
        window.ProgressDialog = factory(window.jQuery, window._ );
    }
})(function($, _) {
    var dialogTemplates =
        "<div>" +
            "<div class='messages-list'></div>" +
        "</div>";

    var showCounter = 0,
        isShown = false,
        getDialogDefer = null,
        messageList = [];

    function getDialog() {
        if(getDialogDefer == null) {
            getDialogDefer = new $.Deferred();

            var dialog = $(dialogTemplates);

            $(document.body).append(dialog);

            dialog.dialog({
                modal: true,
                autoOpen: false,
                closeOnEscape: false,
                dialogClass: "no-close-button"
            });

            getDialogDefer.resolve(dialog);
        }

        return getDialogDefer.promise();
    }

    function refreshMessages() {
        getDialog().done(function(dialog) {
            var list = dialog.find(".messages-list");

            list.empty();
            for(var message in messageList) {
                list.append(messageList[message].$ele);
            }

            if(list.children().length == 0) {
                dialog.dialog("close");
            } else {
                dialog.dialog("open");
            }
        });
    }

    function DialogMessage(options) {
        var opts = {};
        if($.type(options) == "undefined") {
            opts.message = ProgressDialog.defaults.message;
        }

        if($.type(options) == "string") {
            opts.message = options;
        }

        this.options = $.extend({}, opts);

        this.$ele = $("<div/>", { "class": "message", text: this.options.message }).css("text-align","center");
    }

    $.extend(DialogMessage.prototype, {
        message: function(opts) {
            if($.type(opts) == "string") {
                this.options.message = opts;
                this.refresh();
                return this;
            }
            else
                return this.options.message;
        },
        refresh: function() {
            this.$ele.text(this.options.message);
            return this;
        },
        close: function() {
            messageList = _.filter(messageList, function(x) {
                return this != x;
            }, this);

            refreshMessages();

            return this;
        }
    });

    var ProgressDialog = {
        defaults: {
            message: "กำลังดำเนินการ"
        },
        alert: function(message) {
            alert(message);
        },
        prompt: function(message, value) {
            return prompt(message, value);

        },
        confirm: function(message) {
            return confirm(message);
        },
        show: function() {

        },
        message: function(options) {
            var message = new DialogMessage(options)
            messageList.push(message);

            refreshMessages();

            return message;
        }
    };

    return ProgressDialog;
});
